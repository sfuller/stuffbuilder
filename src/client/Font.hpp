#ifndef FONT_HPP
#define FONT_HPP

#include <string>
#include <ft2build.h>
#include FT_FREETYPE_H

#include <stuffbuilder/GlyphMetricProperties.hpp>

class Font
{
    char* _image;
    stuffbuilder::GlyphMetricProperties properties[256];

    FT_Library _library;
    
    int _pixel_size;
    
    FT_Error _init_error;

    void WriteToImage(int x, int y, FT_Bitmap* bitmap);
    void FlipImage();

public:
    Font();
    ~Font();
    
    FT_Error get_error() const;
    
    int get_pixel_size() const;

    int Convert(const std::string& string);
    
    const char* get_image_data() const;
    
    const stuffbuilder::GlyphMetricProperties& get_glyph_properties(int index) const;
    
    
    
    
};

#endif // FONT_HPP
