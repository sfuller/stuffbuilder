//
//  main.cpp
//  stuffbuilder
//
//  Created by Sam Fuller on 12/21/14.
//
//

#include "Font.hpp"
#include <iostream>
#include <Targa.hpp>
#include <fstream>

using namespace std;
using namespace OMGTarga;
using namespace stuffbuilder;

int main(int argc, char** argv)
{
    if(argc < 3)
    {
        cout << "Usage: Filename TargaFileName [CharacterInfoJsonFileName]" << endl;
        return 1;
    }
    
    const char* filename = argv[1];
    const char* targa_name = argv[2];
    
    Font font;
    if(font.get_error())
    {
        return -1;
    }
    
    int result = font.Convert(filename);
    if(result) {
        return result;
    }
    
    int size = font.get_pixel_size() * 16;
    
    const char* image_data = font.get_image_data();
    char* out_data = new char[size * size * 4];
    for(int row = size - 1; row >= 0; --row)
    {
        for(int col = 0; col < size; ++col)
        {
            int i = (size - row - 1) * size + col;
            char value = image_data[row * size + col];
            out_data[i * 4] = 255;
            out_data[i * 4 + 1] = 255;
            out_data[i * 4 + 2] = 255;
            out_data[i * 4 + 3] = value;
        }
    }
    
    Targa targa;
    targa.set_image_data(out_data, size, size, ByteOrder::BGR, ScreenOrigin::BottomLeft);
    targa.save_to_file(targa_name);
    
    
    if(argc > 3)
    {
        const char* json_filename = argv[3];
        mjson::Value root;
        for(int i = 0; i < 256; ++i)
        {
            mjson::Value char_val;
            GlyphMetricProperties::to_json(char_val, font.get_glyph_properties(i));
            root.Push(char_val);
        }
        std::string out_string = root.ToJSONString();
        ofstream out_stream(json_filename);
        if(!out_stream) {
            return 1;
        }
        out_stream.write(out_string.c_str(), out_string.size());
        out_stream.close();
    }
    
    return 0;
}

