//
// Created by Sam Fuller on 7/3/15.
//

#include <stuffbuilder/FontProperties.hpp>

using namespace stuffbuilder;

const GlyphMetricProperties& FontProperties::property(char index) {
    return _properties[index];
}

void FontProperties::from_json(const mjson::Value &value, FontProperties &out) {
    for(int i = 0; i < 256; ++i) {
        GlyphMetricProperties::from_json(value.GetAtIndex(i), out._properties[i]);
    }
}
