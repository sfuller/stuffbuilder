#include "Font.hpp"
#include <fstream>
#include <iostream>
#include <sstream>

#include FT_BITMAP_H

using namespace std;

Font::Font()
:_image(NULL),
_pixel_size(32)
{
    _init_error = FT_Init_FreeType(&_library);
    if(_init_error)
    {
        cerr << "Failed to initialize freetype";
    }

}

Font::~Font()
{
    FT_Done_FreeType(_library);
    delete [] _image;
}

FT_Error Font::get_error() const {
    return _init_error;
}

int Font::get_pixel_size() const {
    return _pixel_size;
}

int Font::Convert(const std::string& path)
{
    FT_Error error;
    FT_Face face;
    error = FT_New_Face(_library, path.c_str(), 0, &face);
    if(error)
    {
        if( error == FT_Err_Unknown_File_Format) {
            cerr << "Error: Unknown format." << endl;
        }
        else {
            cerr << "Failed to read file " << path.c_str() << endl;
        }
        return 1;
    }

    error = FT_Set_Pixel_Sizes(face, 0, _pixel_size);
    if(error)
    { cerr << "Failed to set pixel size." << endl; throw 3; }

    int px = 0;
    int py = 0;

    _image = new char[_pixel_size * _pixel_size * 256];

    for(int n=0; n<256; n++)
    {
        FT_UInt glyphIndex = FT_Get_Char_Index(face,n);
        error = FT_Load_Glyph(face,glyphIndex,FT_LOAD_DEFAULT);
        if(error) cerr << "Could not load glyph of ascii index " << n << "." << endl;
        FT_Glyph_Metrics metrics = face->glyph->metrics;
        properties[n].bearingX = (float)(metrics.horiBearingX /64) / (float)_pixel_size;
        properties[n].bearingY = (float)(metrics.horiBearingY /64) / (float)_pixel_size;
        properties[n].width = (float)(metrics.width /64) / (float)_pixel_size;
        properties[n].height = (float)(metrics.height /64) / (float)_pixel_size;
        properties[n].advance = (float)(metrics.horiAdvance /64) / (float)_pixel_size;

        error = FT_Render_Glyph(face->glyph,FT_RENDER_MODE_NORMAL);
        if(error) cerr << "Could not render glyph of ascii index " << n << "." << endl;
        py = (n / 16) * _pixel_size;
        px = (n % 16) * _pixel_size;
        WriteToImage(px,py,&face->glyph->bitmap);
    }
    
    return 0;
}

void Font::WriteToImage(int x, int y, FT_Bitmap *bitmap)
{
    FT_Bitmap_Convert(_library,bitmap,bitmap,1);
    int cx = 0; int cy = 0;
    while(cx+(cy*bitmap->pitch) < bitmap->pitch * bitmap->rows)
    {
        char value = bitmap->buffer[cx+(cy*bitmap->pitch)];
        int index = cx+x + ( (cy + y) * _pixel_size * 16);
        _image[index] = value;
        cx++;
        if(cx >= bitmap->pitch) {cx=0; cy++;}
    }
}

const char* Font::get_image_data() const {
    return _image;
}

const stuffbuilder::GlyphMetricProperties& Font::get_glyph_properties(int index) const {
    return properties[index];
}
