//
// Created by Sam Fuller on 6/27/15.
//

#include <stuffbuilder/GlyphMetricProperties.hpp>

using namespace stuffbuilder;
using namespace mjson;

void GlyphMetricProperties::to_json(mjson::Value& out, const GlyphMetricProperties &properties) {
    out.SetValueForKey("bearing_x", Value(properties.bearingX));
    out.SetValueForKey("bearing_y", Value(properties.bearingY));
    out.SetValueForKey("width", Value(properties.width));
    out.SetValueForKey("height", Value(properties.height));
    out.SetValueForKey("advance", Value(properties.advance));
}

void GlyphMetricProperties::from_json(const mjson::Value& value, GlyphMetricProperties& out) {
    out.bearingX = value.GetWithKey("bearing_x").ToFloat();
    out.bearingY = value.GetWithKey("bearing_y").ToFloat();
    out.width = value.GetWithKey("width").ToFloat();
    out.height = value.GetWithKey("height").ToFloat();
    out.advance = value.GetWithKey("advance").ToFloat();
}