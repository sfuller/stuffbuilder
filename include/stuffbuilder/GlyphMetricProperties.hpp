//
//  GlyphData.hpp
//  stuffbuilder
//
//  Created by Sam Fuller on 1/25/15.
//
//

#ifndef stuffbuilder_GlyphData_hpp
#define stuffbuilder_GlyphData_hpp

#include <mjson.hpp>

namespace stuffbuilder {

    class GlyphMetricProperties
    {
    public:
        float bearingX, bearingY, width, height, advance;

        static void from_json(const mjson::Value& value, GlyphMetricProperties& properties);
        static void to_json(mjson::Value& out, const GlyphMetricProperties& properties);
    };

}

#endif
