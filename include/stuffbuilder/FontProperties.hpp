//
// Created by Sam Fuller on 7/3/15.
//

#ifndef METALANCER_FONTPROPERTIES_H
#define METALANCER_FONTPROPERTIES_H

#include <mjson.hpp>
#include <stuffbuilder/GlyphMetricProperties.hpp>

namespace stuffbuilder {
    class FontProperties {

       GlyphMetricProperties _properties[256];

    public:


        const GlyphMetricProperties& property(char index);

        static void from_json(const mjson::Value& value, FontProperties& out);
        static void to_json(const FontProperties& in, mjson::Value& out);

    };
}


#endif //METALANCER_FONTPROPERTIES_H
